//
//  PodViewController.swift
//  Podissues
//
//  Created by Daniel Li on 9/9/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit
import RealmSwift

class PodViewController: UITableViewController {
    
    var pod = Pod()
    
    var issues: Results<Issue> {
        get {
            return Realm().objects(Issue)
        }
    }
    
    var buildings = [Building]()
    
    @IBAction func logoutButton(sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let logoutAction = UIAlertAction(title: "Logout", style: .Destructive) { (action) in
            self.refreshControl?.endRefreshing()
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(false, forKey: "loggedIn")
            defaults.setObject("", forKey: "podID")
            
            let realm = Realm()
            realm.write {
                realm.deleteAll()
            }
            self.tableView.reloadData()
            self.performSegueWithIdentifier("logout", sender: self)
        }
        alertController.addAction(logoutAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up UIRefreshControl
        refreshControl?.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        
        // Fix refresh control glitch where tableview will be at a lower position on load
        tableView.contentOffset = CGPointMake(0, 0 - tableView.contentInset.top)
        
        // Dynamic row height
        tableView.rowHeight = 100.0
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        // Fixes swipe back to tableView selection glitch
        if let selectedIndexPath = tableView.indexPathForSelectedRow() {
            tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.boolForKey("loggedIn") {
            if defaults.stringForKey("podID") == nil || defaults.stringForKey("podID") == "" {
                performSegueWithIdentifier("setup", sender: self)
            } else {
                // maybe insert error if pod not found, or just quit/logout
                if let realmPod = Realm().objectForPrimaryKey(Pod.self, key: defaults.stringForKey("podID")!) {
                    pod = realmPod
                }
//                self.title = "Pod \(pod.name)'s Issues"
                self.title = "Test Pod's Issues"
            }
        } else {
            performSegueWithIdentifier("login", sender: self)
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if tableView.contentOffset.y <= -215.0 {
//            refreshControl?.attributedTitle = NSAttributedString(string: "Release to refresh")
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issues.count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("issueCell", forIndexPath: indexPath) as! PodIssueCell
        cell.titleLabel.text = issues[indexPath.row].title
        cell.descriptionLabel.text = issues[indexPath.row].desc
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Navigation
    
    @IBAction func cancelledAddIssue(segue: UIStoryboardSegue) {    }
    
    @IBAction func addedIssue(segue: UIStoryboardSegue) {    }
    
    @IBAction func loggedIn(segue: UIStoryboardSegue) {    }
    
    @IBAction func finishedSetup(segue: UIStoryboardSegue) {
        getIssues()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let destination = segue.destinationViewController as? IssueTableViewController {
            if let selectedCellIndexPath = tableView.indexPathForSelectedRow() {
                destination.issue = issues[selectedCellIndexPath.row]
            }
            
            // Make back button for IssueTableViewController say "Pod"
            let backItem = UIBarButtonItem()
            backItem.title = "Pod"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    // Data
    
    func refresh(refreshControl: UIRefreshControl) {
        
        getIssues()
        
//        var delayInSeconds = 0.5;
//        var popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)));
//        dispatch_after(popTime, dispatch_get_main_queue()) { () -> Void in
//            // When done requesting/reloading/processing invoke endRefreshing, to close the control
//        }
    }
    
    func getIssues() {
        // Retrieve issue data from NSUserDefaults
        let defaults = NSUserDefaults.standardUserDefaults()
        let id = defaults.objectForKey("podID") as! String
        
        DataManager.getPodData(id, completion: { (data, error) -> Void in

            if let error = error {
                println("error code: \(error.code)")
                
                let title: String
                let message: String
                
                if error.code == -1004 {
                    title = "Connection Error"
                    message = "Please check your internet connection"
                }
                else if error.code == -1001 {
                    title = "Server Error"
                    message = "Unable to connect to the server. Please try again later."
                } else {
                    title = "Unknown Error"
                    message = "Unable to connect: unknown error."
                }
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
                alertController.addAction(OKAction)
                
                self.presentViewController(alertController, animated: true) {
                    // ...
                }
                
            } else {
                let json = JSON(data: data!)
                if let issueArray = json.array {
                    println("Reading issue array...")
                    
                    // Convert json into model objects
                    for issueDict in issueArray {
                        
                        // Create issue model
                        let issueToCreate = Issue()
                        issueToCreate.title = issueDict["name"].string!
                        issueToCreate.desc = issueDict["desc"].string!
                        issueToCreate.id = issueDict["id"].string!
                        
                        // Get comments and create comment models
                        let comments = List<Comment>()
                        if let commentJSON = issueDict["comments"].array {
                            for comment in commentJSON {
                                let commentToCreate = Comment()
                                commentToCreate.text = comment["content"].string!
                                commentToCreate.id = comment["id"].string!
                                // var date = comment[""].string
                                comments.append(commentToCreate)
                            }
                        }
                        issueToCreate.comments.extend(comments)
                        
                        // Write issue to realm, update if exists
                        let realm = Realm()
                        realm.write {
                            realm.add(issueToCreate, update: true)
                        }
                    }
                }
                self.tableView.reloadData()
                self.refreshControl!.endRefreshing()
            }
        })
    }
    
    
    
}
