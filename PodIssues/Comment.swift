//
//  Comment.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/12/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import Foundation
import RealmSwift

class Comment: Object {
    dynamic var text: String = "Empty Comment"
    dynamic var id: String = "null"
//    var date: NSDate
    
    override static func primaryKey() -> String {
        return "id"
    }
    
}