//
//  Issue.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/11/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import Foundation
import RealmSwift

class Issue: Object {
    dynamic var title: String = "Untitled Issue"
    dynamic var desc: String = ""
    dynamic var id: String = "null"
    let comments = List<Comment>()
    
//    var date: NSDate
    
    override static func primaryKey() -> String? {
        return "id"
    }
}