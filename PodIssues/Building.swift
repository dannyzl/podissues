//
//  Building.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/10/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import Foundation

class Building {
    var name: String
    var id: String
    var pods = [Pod]()
    
    init(name: String, id: String, pods: [Pod]) {
        self.name = name
        self.id = id
        self.pods = pods
    }
    
    init() {
        self.name = "Untitled Building"
        self.id = "0"
    }
}