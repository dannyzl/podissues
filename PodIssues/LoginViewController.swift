//
//  LoginViewController.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/9/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit
import RealmSwift

class LoginViewController: UIViewController {
    
    var buildings = [Building]()
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginButton(sender: UIButton) {
        
        // Insert code for determine if login
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: "loggedIn")
        defaults.setObject("0", forKey: "podID")
        
        
        // Test case
        let realm = Realm()
        realm.write {
            let issue = Issue()
            issue.title = "Stop pooping on the toilet."
            issue.desc = "I seriously hope it's evan because the last time someone pood it was him and we would all be really confused and angry if there was another pooper on board. Please please be evan pls respond evan."
            issue.id = "0"
            
            let comment1 = Comment()
            comment1.text = "Omfg this is great."
            comment1.id = "100"
            
            let comment2 = Comment()
            comment2.text = "It's evan. Definitely was not me. It was trey. "
            comment2.id = "101"
            
            let comment3 = Comment()
            comment3.text = "You idiot you're not supposed to say who you are, this is anonymous. Ever heard of yik yak? God damn it this is one verbose comment."
            comment3.id = "102"
            
            issue.comments.append(comment1)
            issue.comments.append(comment2)
            issue.comments.append(comment3)
            
            realm.add(issue)
        }
        
        performSegueWithIdentifier("loggedIn", sender: self)
    }
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var testFirstTimeLoginButton: UIButton!
    @IBAction func testFirstTimeLoginButton(sender: UIButton) {
        
        // Login
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: "loggedIn")
        
        testFirstTimeLoginButton.enabled = false
        performSegueWithIdentifier("loggedInNoob", sender: self)
    }
    
    @IBAction func createAccountButton(sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        loadingIndicator.hidden = true
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Pass buildings array to building selector
        if segue.identifier == "loggedInNoob" {
            let destination = segue.destinationViewController.topViewController as! BuildingSelectViewController
            destination.buildings = buildings
        }
    }
}
