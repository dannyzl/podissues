//
//  Pod.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/10/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import Foundation
import RealmSwift

class Pod: Object {
    dynamic var name: String = "Untitled Pod"
    dynamic var id: String = "Untitled Pod"
    dynamic var memberCount: Int = 0
    let issues = List<Issue>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}