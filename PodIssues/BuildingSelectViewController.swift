//
//  BuildingSelectViewController.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/10/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit

class BuildingSelectViewController: UITableViewController, NSURLConnectionDelegate {
    
    var buildings = [Building]()
    
    var selectedBuildingIndex: Int!
    
    @IBOutlet weak var nextButton: UIBarButtonItem!
    @IBAction func nextButton(sender: UIBarButtonItem) {
        performSegueWithIdentifier("selectedBuilding", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 60.0
        
        LoadingOverlay.shared.showOverlay(self.navigationController?.view)
        
        retrieveBuildingData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return buildings.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("buildingCell", forIndexPath: indexPath) as! UITableViewCell
        
        // Setup cell text
        cell.textLabel!.text = buildings[indexPath.row].name
        if buildings[indexPath.row].pods.count == 1 {
            cell.detailTextLabel!.text = "1 pod"
        } else {
            cell.detailTextLabel!.text = "\(buildings[indexPath.row].pods.count) pods"
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // Visually deselect the row immediately
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        // Uncheck the previously selected row if there was one
        if let selectedIndex = selectedBuildingIndex {
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: selectedIndex, inSection: 0))
            cell?.accessoryType = .None
        }
        
        // Make selected row's building the selected building
        selectedBuildingIndex = indexPath.row
        
        // Checkmark if building is selected
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = .Checkmark
        
        // Enable next button
        nextButton.enabled = true
        
    }
    
    
    @IBAction func unwindToBuildingSelect(segue: UIStoryboardSegue) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if let destination = segue.destinationViewController as? PodSelectViewController {
            destination.building = self.buildings[selectedBuildingIndex]
        }
    }
    
    func retrieveBuildingData() {
        // Retrieve building data from server
        DataManager.getBuildingData { (data, error) -> Void in
            if let error = error {
                
                println("error code: \(error.code)")
                
                let title: String
                let message: String
                
                if error.code == -1004 {
                    title = "Connection Error"
                    message = "Please check your internet connection"
                } else if error.code == -1001 {
                    title = "Server Error"
                    message = "Unable to connect to the server. Please try again later."
                } else {
                    title = "Unknown Error"
                    message = "Unable to connect: unknown error."
                }
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                    self.retrieveBuildingData()
                }
                alertController.addAction(OKAction)
                
                self.presentViewController(alertController, animated: true) {
                    // ...
                }
                
            } else {
                let json = JSON(data: data!)
                if let buildingArray = json.array {
                    println("Reading array...")
                    // Convert json into model objects
                    for buildingDict in buildingArray {
                        var name = buildingDict["name"].string
                        var id = buildingDict["id"].string
                        var pods = [Pod]()
                        if let podsJSON = buildingDict["pods"].array {
                            for pod in podsJSON {
                                var podToCreate = Pod()
                                if let name = pod["name"].string {
                                    podToCreate.name = name
                                }
                                if let podID = pod["id"].string {
                                    podToCreate.id = podID
                                }
                                if let memberCount = pod["member_count"].int {
                                    podToCreate.memberCount = memberCount
                                }
                                pods.append(podToCreate)
                            }
                        }
                        self.buildings.append(Building(name: name!, id: id!, pods: pods))
                    }
                    LoadingOverlay.shared.hideOverlayView()
                    self.tableView.reloadData()
                    println("Reloaded tableView")
                }
            }
        }
    }
    
    
}
