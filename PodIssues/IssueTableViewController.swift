//
//  IssueTableViewController.swift
//  PodIssues
//
//  Created by Daniel Li on 9/12/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit
import RealmSwift

class IssueTableViewController: UITableViewController {

    var issue: Issue!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up tableView height
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 160.0
        
        title = "Issue"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1 + issue.comments.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("issueCell", forIndexPath: indexPath) as! IssueCell
            cell.titleLabel.text = issue.title
            cell.descriptionLabel.text = issue.desc
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("commentCell", forIndexPath: indexPath) as! CommentCell
            cell.contentLabel.text = issue.comments[indexPath.row - 1].text
            return cell
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
}
