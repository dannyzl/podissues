//
//  AppDelegate.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/6/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        let realm = Realm()
        realm.write {
            let issue = Issue()
            issue.title = "Someone left a towel in the shower in our pod on the rail."
            issue.desc = "This towel was on the rail. I left it outside on the hanger. Somebody get it before the janitor does!"
            issue.id = "1"
            
            let comment1 = Comment()
            comment1.text = "It's probably Dan's."
            comment1.id = "200"
            
            let comment2 = Comment()
            comment2.text = "Oh shit I'll get it lol."
            comment2.id = "201"
            
            let comment3 = Comment()
            comment3.text = "Okay hurry up cause I think the janitor comes at like 2"
            comment3.id = "202"
            
            let comment4 = Comment()
            comment4.text = "No I think it comes at like 4"
            comment4.id = "203"
            
            issue.comments.append(comment1)
            issue.comments.append(comment2)
            issue.comments.append(comment3)
            issue.comments.append(comment4)
            
            realm.add(issue, update: true)
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

