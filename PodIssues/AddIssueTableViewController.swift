//
//  AddIssueTableViewController.swift
//  PodIssues
//
//  Created by Daniel Li on 9/9/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit

class AddIssueTableViewController: UITableViewController {
    
//    var issueTitle: String = ""
//    var issueDetails: String = ""
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBAction func titleTextFieldChanged(sender: UITextField) {
        // Enable/disable add button if text is empty
        if titleTextField.text.isEmpty {
            addButton.enabled = false
        } else {
            addButton.enabled = true
        }
    }
    
    @IBOutlet weak var detailsTextField: UITextField!
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBAction func addButton(sender: UIBarButtonItem) {
        // Insert add issue implementation here or in prepareForSegue method
        performSegueWithIdentifier("addedissue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.toolbarHidden = false
        let commentField = UITextField()
        self.navigationController?.toolbarItems?.append(commentField)
        
        // Default disable Add button if there is no text in title launch
        if titleTextField.text == "" {
            addButton.enabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 1
    }

    // Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "cancelledAddissue" {
            
        } else if segue.identifier == "addedissue" {
            // Strip title text field string of whitespaces
            titleTextField.text = titleTextField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//            issueTitle = titleTextField.text
//            issueDetails = detailsTextField.text
        }
    }
}
