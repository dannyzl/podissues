//
//  PodSelectViewController.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/10/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import UIKit
import RealmSwift

class PodSelectViewController: UITableViewController {
    
    var building: Building!
    var selectedPodIndex: Int!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBAction func doneButton(sender: UIBarButtonItem) {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(building.pods[selectedPodIndex].id as String, forKey: "podID")
        println("Pod id from podselectviewcontroller")
        println(building.pods[selectedPodIndex].id)
        
        let realm = Realm()
        realm.write {
            realm.add(self.building.pods[self.selectedPodIndex], update: true)
        }
        
        self.performSegueWithIdentifier("finishedSetupToHome", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 60.0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return building.pods.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("podCell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = building.pods[indexPath.row].name
        if building.pods[indexPath.row].memberCount == 1 {
            cell.detailTextLabel?.text = "1 member"
        } else {
            cell.detailTextLabel?.text = "\(building.pods[indexPath.row].memberCount) members"
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == building.pods.count {
            
        } else {
            // Visually deselect the row
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            
            if let selectedIndex = selectedPodIndex {
                let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: selectedIndex, inSection: 0))
                cell?.accessoryType = .None
            }
            
            // Make selected row's pod the selected pod
            selectedPodIndex = indexPath.row
            
            // Checkmark if pod is selected
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            cell?.accessoryType = .Checkmark
            
            // Enable next button
            doneButton.enabled = true
            
            println("Selected \(building.pods[indexPath.row].name) pod")
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
}
