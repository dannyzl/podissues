//
//  DataManager.swift
//  PodComplaints
//
//  Created by Daniel Li on 9/10/15.
//  Copyright (c) 2015 Cornell University. All rights reserved.
//

import Foundation

let dataURL = "http://10.147.13.152/"

class DataManager {
    
    class func getBuildingData(completion: ((data: NSData?, error: NSError?) -> Void)) {
        println("Getting building data...")
        loadDataFromURL(NSURL(string: dataURL + "list_basic_building_and_pod_info")!, completion: {(data, error) -> Void in
            if let urlData = data {
                println("Data acquired")
                completion(data: urlData, error: nil)
            }
            if let err = error {
                println("Server returned error:")
                println(err.localizedDescription)
                completion(data: nil, error: error)
            }
        })
    }
    
    class func getPodData(id: String, completion: ((data: NSData?, error: NSError?) -> Void)) {
        println("Getting issue data...")
        loadDataFromURL(NSURL(string: dataURL + "list_issues_in_pod/" + id)!, completion: {(data, error) -> Void in
            if let urlData = data {
                println("Data acquired")
                completion(data: urlData, error: nil)
            }
            if let err = error {
                println("Server returned error:")
                println(err.localizedDescription)
                completion(data: nil, error: error)
            }
        })
    }
    
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        var session = NSURLSession.sharedSession()
        
        session.configuration.timeoutIntervalForRequest = 3.0
        session.configuration.timeoutIntervalForResource = 3.0
        
        // Use NSURLSession to get data from an NSURL
        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
            println("Started dataTask")
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    var statusError = NSError(domain:"", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                    println("status code 200!")
                }
            } else {
                println("no response")
            }
        })
        loadDataTask.resume()
    }
}